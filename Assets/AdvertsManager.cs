using UnityEngine;
using System;

public class AdvertsManager : MonoBehaviour
{
    [SerializeField] AdManagerState _adManagerEditorTestingState = AdManagerState.Normal;

    public bool AdsRemoved = false;

    public static AdvertsManager Instance;

    public event Action OnRewardedVideoRewarded;
    public event Action OnRewardedVideoRewardedUpdate;

    private void Awake()
    {
        if(Instance)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }

    void Start()
    {
#if UNITY_ANDROID
        //CHANGE THIS TO THE CORRECT ID
        //also change the admob app id in the ironsource mediation settings
        IronSource.Agent.init("78037c45");
#elif UNITY_IOS
        //CHANGE THIS TO THE CORRECT ID
        //also change the admob app id in the ironsource mediation settings
        IronSource.Agent.init("780342bd");
#endif

        IronSource.Agent.validateIntegration();

        IronSource.Agent.shouldTrackNetworkState(true);

        PrepareSourceEvents();
    }

    void PrepareSourceEvents()
    {
        IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
        IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
        IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;

        if(!AdsRemoved)
        {
            IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
            ShowBanner();
        }
    }

    public void ShowBanner()
    {
        IronSource.Agent.displayBanner();
    }

    public void HideBanner()
    {
        IronSource.Agent.hideBanner();
    }

    public void ShowInterstitial()
    {
        IronSource.Agent.showInterstitial();
    }

    public void ShowRewardedVideo()
    {
        if(AdsRemoved)
        {
            GiveReward();
        }

        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            IronSource.Agent.showRewardedVideo("DefaultRewardedVideo");
        }
        else
        {
#if UNITY_EDITOR
            if (_adManagerEditorTestingState == AdManagerState.Normal || _adManagerEditorTestingState == AdManagerState.AlwaysSuccess)
            {
                GiveReward();
            }
            else
            {
                Debug.LogError("No reward");
            }
#endif
        }
    }

    void RewardedVideoAdOpenedEvent()
    {
    }

    void RewardedVideoAdClosedEvent()
    {
    }

    void RewardedVideoAvailabilityChangedEvent(bool available)
    {
        //Change the in-app 'Traffic Driver' state according to availability.
    }

    void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
    {
        GiveReward();
    }

    void RewardedVideoAdShowFailedEvent(IronSourceError error)
    {

    }

    public void RemoveAdsBought()
    {
        AdsRemoved = true;
        IronSource.Agent.hideBanner();
        IronSource.Agent.destroyBanner();
    }

    private void GiveReward()
    {
        if (OnRewardedVideoRewarded != null)
            OnRewardedVideoRewarded();

        if (OnRewardedVideoRewardedUpdate != null) OnRewardedVideoRewardedUpdate();
    }
}

public enum AdAnalyticEnum
{
    Request,
    Opened,
    Failed,
    Complete,
}

public enum AdManagerState
{
    Normal,
    AlwaysFail,
    AlwaysSuccess
}
